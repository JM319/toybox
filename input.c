#include "input.h"

static int screen_width;
static int screen_height;

void initialize_input(int width, int height)
{
    screen_width = width;
    screen_height = height;
}

void update_input(struct input_system *input)
{
    struct game_input *old_input = &input->static_input;
    struct game_input new_input = input->dynamic_input;

    old_input->was_down = old_input->is_down;

    old_input->is_down = new_input.is_down;
    old_input->cursor_x = new_input.cursor_x;
    old_input->cursor_y = new_input.cursor_y;
}

void normalize_input(struct input_system *input)
{
    const float w = (float) screen_width / 2.f;
    const float h = (float) screen_height / 2.f;
    input->static_input.cursor_x = (input->static_input.cursor_x - w) / w;
    input->static_input.cursor_y = -(input->static_input.cursor_y - h) / h;
}

void
set_cursor_position(struct input_system *input,
                    float x, float y)
{
    input->dynamic_input.cursor_x = x;
    input->dynamic_input.cursor_y = y;
}

void set_mouse_down(struct input_system *input,
                    bool is_down)
{
    input->dynamic_input.is_down = is_down;
}

