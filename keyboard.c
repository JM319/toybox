#include "toybox.h"

static struct white_key keys[23];
static struct black_key black_keys[17];

/* TODO: This stuff needs a central definition */
#define RED 1.,0.,0.
#define BLACK 0.,0.,0.
#define WHITE 1.,1.,1.
#define GREEN 0.,1.,0.
#define YELLOW 1.,1.,0.
/* TODO: This stuff needs a central definition */

enum key_offset
{
    KO_LEFT,
    KO_MIDDLE,
    KO_RIGHT
};

enum key_offset get_offset(size_t key_index)
{
    static enum key_offset off_pattern[7] =
            {
                    KO_LEFT, KO_MIDDLE, KO_MIDDLE, KO_RIGHT, KO_LEFT, KO_MIDDLE, KO_RIGHT
            };

    return off_pattern[key_index % 7];
}

static inline float
key_width(size_t num_keys)
{
    return 1.6f / num_keys;
}

static float
get_key_length(size_t num_keys)
{
    return 6 * key_width(num_keys) / (7.f/8.f);
}

static float
get_black_key_length(size_t num_keys)
{
    return 4 * key_width(num_keys) / (7.f/8.f);
}

static void
initialize_black_key(struct game_state *state,
                     size_t key_index,
                     size_t black_key_index)
{
    struct white_key *key = &keys[key_index];
    struct black_key *black_key = &black_keys[black_key_index];
    struct key *white_key = &state->keys[key_index];
    struct key *gen_black_key = &state->keys[ARRAY_COUNT(keys) + black_key_index];

    gen_black_key->pitch = white_key->pitch;
    gen_black_key->pitch.flag |= SHARP;

    black_key->rect.y = key->tip.y;
    black_key->rect.x = key->tip.x + key->tip.width;
    black_key->rect.height = get_black_key_length(ARRAY_COUNT(keys));

    if (key_index == sizeof(keys) / sizeof(keys[0]) - 1)
    {
        black_key->rect.width = 1.f - black_key->rect.x;
    }
    else
    {
        struct white_key *next_key = &keys[key_index + 1];
        black_key->rect.width = next_key->tip.x - black_key->rect.x;
    }
    glGenBuffers(1, &black_key->buffer);
    glBindBuffer(GL_ARRAY_BUFFER, black_key->buffer);
    GLfloat points[8];
    rectangle_vertices(&black_key->rect, points);
    glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

    gen_black_key->key_drawable = black_key;
    gen_black_key->type = BLACK_KEY;
}

static void initialize_key(struct game_state *state, size_t key_index)
{
    struct white_key *key = &keys[key_index];
    struct key *general_key = &state->keys[key_index];

    static const float starting_offset = -0.6f;
    static const size_t num_keys = ARRAY_COUNT(keys);
    const float width = key_width(ARRAY_COUNT(keys));
    const float left = starting_offset + key_index * width;
    const enum key_offset offset_type = get_offset(key_index);

    /*
     *                            _       y2
     *                           |  |
     *                           |  |
     *  left + offset           _|  |_    y1
     *                         |     |
     *                         |     |
     *                         |     |
     *  left                   |_____|    y0
     *                        /|     |\
     *                      x0 x1   x2 x3
     */
    GLfloat minor_width = (3.f/4) * width;

    GLfloat offset;
    if (offset_type == KO_LEFT)
        offset = 0.0f;
    else if (offset_type == KO_MIDDLE)
    {
        minor_width = .5f * width;
        offset = (width - minor_width) / 2;
    }
    else
    {
        offset = width - minor_width;
    }

    const GLfloat height = get_key_length(num_keys);
    const GLfloat black_key_length = get_black_key_length(num_keys);

    const GLfloat x0 = left; const GLfloat x1 = x0 + offset;
    const GLfloat x2 = x1 + minor_width; const GLfloat x3 = x0 + width;
    const GLfloat y0 = -.999f; const GLfloat y1 = y0 + height - black_key_length    ;
    const GLfloat y2 = y0 + height;

    key->base.x = x0;  key->base.width = width;
    key->base.y = y1;  key->base.height = height;

    key->tip.x = x1;            key->tip.width = minor_width;
    key->tip.y = y0 + height;   key->tip.height = black_key_length;

    GLfloat base_points[8];
    GLfloat tip_points[8];
    rectangle_vertices(&key->base, base_points);
    rectangle_vertices(&key->tip,  tip_points);

    GLfloat outline[18];
    outline[0]  = x0; outline[1] = y0;
    outline[2]  = x0; outline[3] = y1;
    outline[4]  = x1; outline[5] = y1;
    outline[6]  = x1; outline[7] = y2;
    outline[8]  = x2; outline[9] = y2;
    outline[10] = x2; outline[11] = y1;
    outline[12] = x3; outline[13] = y1;
    outline[14] = x3; outline[15] = y0;
    outline[16] = x0; outline[17] = y0;

    glGenBuffers(3, key->buffers);
    glBindBuffer(GL_ARRAY_BUFFER, key->buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(base_points), base_points, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, key->buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(tip_points), tip_points, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, key->buffers[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(outline), outline, GL_STATIC_DRAW);

    /* TODO: Make this configurable */
    static const struct pitch starting_pitch = {'F', 2, 0};
    general_key->pitch = starting_pitch;
    add_to_pitch(&general_key->pitch, 1 + (int)key_index);
}

void initialize_keyboard(struct game_state *state)
{
    _Static_assert(ARRAY_COUNT(keys) + ARRAY_COUNT(black_keys) == ARRAY_COUNT(state->keys),
                   "Wrong number of keys");

    for (size_t key_index = 0; key_index < ARRAY_COUNT(keys); ++key_index)
    {
        initialize_key(state, key_index);
        state->keys[key_index].key_drawable = &keys[key_index];
        state->keys[key_index].type = WHITE_KEY;
    }

    size_t black_key_index = 0;
    for (size_t key_index = 0;
         key_index < ARRAY_COUNT(keys);
         ++key_index)
    {
        const enum key_offset offset_type = get_offset(key_index);
        if (offset_type == KO_RIGHT) continue;
        initialize_black_key(state, key_index, black_key_index++);
    }
}

static bool
key_intersects(struct game_input *input,
               struct white_key *key)
{
    return rectangle_contains_point(&key->tip,  input->cursor_x, input->cursor_y) ||
           rectangle_contains_point(&key->base, input->cursor_x, input->cursor_y);
}

static bool
black_key_intersects(struct game_input *input,
                     struct black_key *key)
{
    return rectangle_contains_point(&key->rect, input->cursor_x, input->cursor_y);
}

static void
set_key_color(struct game_state *state,
              struct key *key, bool mouse_is_down, bool intersects)
{
    if (intersects)
    {
        if (mouse_is_down)
        {
            if (pitch_equals(&key->pitch, &state->whole_note.pitch))
            {
                key->state = KEY_RIGHT;
            }
            else
            {
                key->state = KEY_WRONG;
            }
        }
        else key->state = KEY_TOUCH;
    }
    else key->state = KEY_DEFAULT;
}

void update_keyboard(struct game_state *state)
{
    for (size_t key_index = 0;
         key_index < ARRAY_COUNT(state->keys);
         ++key_index)
    {
        struct key *key = &state->keys[key_index];
        const bool intersecting = key->type == WHITE_KEY ?
                                  key_intersects(&INPUT(state), (struct white_key *)key->key_drawable) :
                                  black_key_intersects(&INPUT(state), (struct black_key *)key->key_drawable);
        set_key_color(state, key, INPUT(state).is_down, intersecting);
        if (intersecting && (INPUT(state).is_down) && (!INPUT(state).was_down))
            execute_listeners(state, key);
        if (key->state == KEY_RIGHT)
            state->found_key = true;
    }
}

static void
render_black_key(struct key *key)
{
    struct black_key *black_key = (struct black_key *)key->key_drawable;
    switch (key->state)
    {
        case KEY_DEFAULT:
            glVertexAttrib3f(1, BLACK);
            break;
        case KEY_TOUCH:
            glVertexAttrib3f(1, YELLOW);
            break;
        case KEY_RIGHT:
            glVertexAttrib3f(1, GREEN);
            break;
        case KEY_WRONG:
            glVertexAttrib3f(1, RED);
            break;
    }
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, black_key->buffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

static void
render_white_key(struct key *key)
{
    struct white_key *white_key = (struct white_key*)key->key_drawable;
    switch (key->state)
    {
        case KEY_DEFAULT:
            glVertexAttrib3f(1, WHITE);
            break;
        case KEY_TOUCH:
            glVertexAttrib3f(1, YELLOW);
            break;
        case KEY_RIGHT:
            glVertexAttrib3f(1, GREEN);
            break;
        case KEY_WRONG:
            glVertexAttrib3f(1, RED);
            break;
    }

    glEnableVertexAttribArray(0);

    /* Drawing the base */
    glBindBuffer(GL_ARRAY_BUFFER, white_key->buffers[0]);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    /* Drawing the tip */
    glBindBuffer(GL_ARRAY_BUFFER, white_key->buffers[1]);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    /* Draw a black outline around the key */
    glVertexAttrib3f(1, BLACK);
    glBindBuffer(GL_ARRAY_BUFFER, white_key->buffers[2]);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_LINE_LOOP, 0, 9);

    glDisableVertexAttribArray(0);
}

void render_key(struct key *key)
{
    if (key->type == WHITE_KEY)
        render_white_key(key);
    else
        render_black_key(key);
}

void
render_keyboard(struct game_state *state)
{
    for (size_t key_index = 0; key_index < ARRAY_COUNT(state->keys); ++key_index)
    {
        struct key *key = &state->keys[key_index];
        render_key(key);
    }
}

