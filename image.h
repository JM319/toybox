#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stddef.h>

struct rectangle
{
    float x, y, width, height;
};

struct rectangle
rect_from_two_corners(float x0, float y0, float x1, float y1);

struct rectangle
rect_from_point_dims(float x, float y, float w, float h);

bool rectangle_contains_point(
    const struct rectangle *rectangle,
    float x, float y
);

float 
rect_left(const struct rectangle* rectangle);

float 
rect_right(const struct rectangle* rectangle);

float 
rect_top(const struct rectangle* rectangle);

float
rect_bottom(const struct rectangle* rectangle);

void rectangle_vertices(const struct rectangle *rectangle,
                        float *vertices);

/* TODO: Should this be staff_object_spec or something? */
struct clef_spec
{
    const char *pathname;                  /* Location of image */
    size_t height_px;                      /* Height in pixels  */
    size_t y_pixel_top, y_pixel_bottom;    /* Px location of vertical glue points */
    struct rectangle uv_coordinates;
    float x_start;                         /* Left-most x position in render coordinates */
    float width;                           /* Width in render coordinates */
};

struct image
{
    struct rectangle rect;
    void *render_data;
};

void
load_clef_spec(const struct clef_spec *spec,
               float top, float bottom,
               struct image *image);

void set_position(struct image *image, 
                  float new_x, float new_y);

void set_image_rect(struct image *image,
                    const struct rectangle *rect);

void set_uv_rect(struct image *image,
                 const struct rectangle *rect);

void load_image(struct image *image,
                const char *pathname,
                const struct rectangle *uv_coords);

void load_image_pixels(struct image *image,
                       const void *pixels,
                       int width, int height, int format,
                       const struct rectangle *uv_coords);

void render_image(struct image *image);

#endif