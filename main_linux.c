#include <stdio.h>
#include "toybox.h"
#include "gl_header.h"

static struct game_state g_state;

static void mouse_callback(GLFWwindow *window, int button, int action, int mods)
{
    (void) window;
    (void) mods;
    if (button == GLFW_MOUSE_BUTTON_LEFT)
        set_mouse_down(&g_state.input,
                       (action == GLFW_PRESS));
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
   (void) scancode;
   (void) mods;
   if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
       glfwSetWindowShouldClose(window, GLFW_TRUE);
}

static void get_user_input(struct game_state *state,
                           GLFWwindow *window)
{
    glfwPollEvents();
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    set_cursor_position(&state->input, (float)x, (float)y);
}

int main(int argc, char *argv[]) {
    (void) argc;
    (void) argv;

    if (!glfwInit()) 
    {
       printf("Unable to init glfw\n");
       return 1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    GLFWwindow *window = glfwCreateWindow(640, 480, "Toybox", NULL, NULL);
    if (!window)
    {
       fprintf(stderr, "Failed to create window\n");
       return 1;
    }

    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_callback);

    glfwMakeContextCurrent(window);
    glewInit();

    glfwGetFramebufferSize(window, &g_state.width, &g_state.height);
    glViewport(0, 0, g_state.width, g_state.height);
    glClearColor(0.8f, 0.8f, 0.8f, 1.f);

    load_shaders(&g_state.program, &g_state.texture_program);

    if (g_state.program == 0)
    {
        printf("Failure loading shaders\n");
        return 1;
    }

    while (!glfwWindowShouldClose(window))
    {
        get_user_input(&g_state, window);
        toybox_draw(&g_state);
        glfwSwapBuffers(window);
    }

    glfwDestroyWindow(window);
    return 0;
} 