#include <time.h>
#include <assert.h>
#include "toybox.h"
#include "stats.h"

typedef void (*on_keydown)(struct game_state *, struct key *);
static on_keydown listeners[100];
static int num_listeners = 0;

void initialize_usage_stats(struct usage_stats *usage_stats)
{
    usage_stats->notes_correct = 0;
    usage_stats->notes_hit = 0;
    usage_stats->start_time = time(NULL);
}

void update_statistics(struct game_state *game, struct key *key)
{
    struct usage_stats *stats = &game->usage_stats;
    assert(stats->start_time != 0);
    stats->notes_hit++;
    if (key->state == KEY_RIGHT)
        stats->notes_correct++;
}

int compute_accuracy(struct usage_stats *usage_stats)
{
    int accuracy = 0;
    if (usage_stats->notes_hit == 0)
        accuracy = 100;
    else
        accuracy = usage_stats->notes_correct * 100 / usage_stats->notes_hit;
    return accuracy;
}

float compute_speed(struct usage_stats *usage_stats)
{
    const time_t second_elapsed = time(NULL) - usage_stats->start_time;
    return (float)usage_stats->notes_correct / second_elapsed;
}

void register_listener(on_keydown listener)
{
    /* TODO: Bounds check */
    listeners[num_listeners++] = listener;
}

void register_listeners()
{
    register_listener(update_statistics);
}

void execute_listeners(struct game_state *game, struct key *key)
{
    for (int i = 0; i < num_listeners; ++i)
        listeners[i](game, key);
}
