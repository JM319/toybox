#ifndef TOYBOX_STATS_H
#define TOYBOX_STATS_H

#include <time.h>

struct usage_stats
{
    int notes_hit;
    int notes_correct;
    time_t start_time;
};

void initialize_usage_stats(struct usage_stats *usage_stats);

int compute_accuracy(struct usage_stats *usage_stats);
float compute_speed(struct usage_stats *usage_stats);

#endif //TOYBOX_STATS_H
