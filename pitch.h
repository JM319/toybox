#ifndef TOYBOX_PITCH_H
#define TOYBOX_PITCH_H

#include <stdbool.h>

#define SHARP 01u
#define FLAT  02u

struct pitch
{
    char note;
    int octave;
    unsigned flag;
};

struct pitch
normalize_pitch(const struct pitch *pitch);

void add_to_pitch(struct pitch *pitch, int interval);

bool
pitch_equals(const struct pitch *lhs, const struct pitch *rhs);

int
calculate_interval(struct pitch *higher, struct pitch *lower);

#endif //TOYBOX_PITCH_H
