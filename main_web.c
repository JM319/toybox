#include "toybox.h"
#include "gl_header.h"

#include <EGL/egl.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "emscripten.h"
#include "html5.h"

struct Environment
{
    EGLint majorVersion;
    EGLint minorVersion;

    EGLDisplay display;
    EGLSurface surface;
};

/* ============================================================================
 * Globals
 */
static struct Environment g_env = {};

/* ============================================================================
 * Interactive functions
 */
EM_JS(int, canvas_get_width, (void), {
    return document.getElementById('canvas').width;
})

EM_JS(int, canvas_get_height, (void), {
    return document.getElementById('canvas').height;
})

EM_BOOL on_mousedown(int event_type, 
                     const EmscriptenMouseEvent *event, 
                     void *user_data)
{
    (void) event_type;
    (void) event;
    struct input_system *input = (struct input_system *)user_data;
    set_mouse_down(input, true);
    return true;
}

EM_BOOL on_mouseup(int event_type, 
                   const EmscriptenMouseEvent *event, 
                   void *user_data)
{
    (void) event_type;
    (void) event;
    struct input_system *input = (struct input_system *)user_data;
    set_mouse_down(input, false);
    return true;
}

EM_BOOL on_mousemove(int event_type,
                     const EmscriptenMouseEvent *event,
                     void *user_data)
{
    (void) event_type;
    struct input_system *input = (struct input_system *)user_data;
    set_cursor_position(input,
                        (float)event->canvasX, (float)event->canvasY);
    return true;
}

EM_BOOL on_touchdown(int event_type, 
                     const EmscriptenTouchEvent *event, 
                     void *user_data)
{
    (void) event_type;
    struct input_system *input = (struct input_system *)user_data;
    set_mouse_down(input, true);
    set_cursor_position(input,
                        (float)event->touches[0].canvasX,
                        (float)event->touches[0].canvasY);
    return true;
}

EM_BOOL on_touchup(int event_type, 
                   const EmscriptenTouchEvent *event, void *user_data)
{
    (void) event_type;
    struct input_system *input = (struct input_system *)user_data;
    set_mouse_down(input, false);
    set_cursor_position(input,
                        (float)event->touches[0].canvasX,
                        (float)event->touches[0].canvasY);
    return true;
}

bool initGL(struct Environment* env, struct game_state *state)
{
    env->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (env->display == EGL_NO_DISPLAY)
    {
        printf("eglGetDisplay failed\n");
        return false;
    }

    if (!eglInitialize(env->display, &env->majorVersion, &env->minorVersion))
    {
        const EGLint last_error = eglGetError();
        const char* error_msg = (last_error == EGL_BAD_DISPLAY) ? "bad display" : "not initialized";
        printf("eglInitialize failed with error code: %s\n", error_msg);
        return false;
    }

    EGLint numConfigs = 0;
    EGLint attribList[] = 
    {
        EGL_RED_SIZE, 5,
        EGL_GREEN_SIZE, 6,
        EGL_BLUE_SIZE, 5,
        EGL_DEPTH_SIZE, 1,
        EGL_NONE
    };
    EGLConfig config;
    if (!eglChooseConfig(env->display, attribList, &config, 1, &numConfigs))
    {
        printf("eglChooseConfig failed\n");
        return false;
    }

    env->surface = eglCreateWindowSurface(env->display, config, 0, NULL);
    if (env->surface == EGL_NO_SURFACE)
    {
        printf("eglCreateWindowSurface failed\n");
        return false;
    }

    static const EGLint contextAttribList[] = 
    {
        EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE
    };
    EGLContext context = eglCreateContext(env->display, config, EGL_NO_CONTEXT, contextAttribList);
    if (context == EGL_NO_CONTEXT)
    {
        printf("eglCreateContext failed\n");
        return false;
    }

    if (!eglMakeCurrent(env->display, env->surface, env->surface, context))
    {
        printf("eglMakeCurrent failed\n");
        return false;
    }

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    /*glViewport(canvas_get_width() * 3 / 16, canvas_get_height() / 8, 
               state->width, state->height); */
    glViewport(0, 0, state->width, state->height);

    load_shaders(&state->program, &state->texture_program);

    if (state->program == 0 || state->texture_program == 0)
    {
        printf("Failure loading shaders\n");
        return false;
    }
    return true;
}

void draw(void *data)
{
    struct game_state *state = (struct game_state *)data;
    toybox_draw(state);
    eglSwapBuffers(g_env.display, g_env.surface);
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    struct game_state state = {};
    state.height = canvas_get_height();
    state.width = canvas_get_width();

    if (!initGL(&g_env, &state))
    {
        printf("Failed to init GL\n");
        return 1;
    }

    struct input_system *input = &state.input;

    emscripten_set_mousedown_callback("#canvas", input, false, on_mousedown);
    emscripten_set_mouseup_callback("#canvas", input, false, on_mouseup);
    emscripten_set_mousemove_callback("#canvas", input, false, on_mousemove);
    emscripten_set_touchstart_callback("#canvas", input, false, on_touchdown);
    emscripten_set_touchend_callback("#canvas", input, false, on_touchup);
    emscripten_set_main_loop_arg(draw, &state, 0, 1);
    return 0;
}