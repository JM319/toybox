#include "pitch.h"
#include <stdlib.h>
#include <assert.h>

void
add_to_pitch(struct pitch *pitch, int interval)
{
    interval = (interval > 0) ? interval - 1 : interval + 1;

    int octave_delta = 0;
    if (interval >= 0)
    {
        const int notes_below_c = (pitch->note <= 'C') ?
                                  'C' - pitch->note :
                                  'J' - pitch->note;
        if (notes_below_c <= interval && notes_below_c != 0)
            octave_delta = 1 + (interval - notes_below_c) / 7;
    }
    else
    {
        const int notes_above_c = (pitch->note >= 'C') ?
                                  pitch->note - 'C' :
                                  pitch->note - 'A' + 5;
        assert(notes_above_c >= 0);

        if (notes_above_c < abs(interval))
            octave_delta = (1 + (abs(interval) - notes_above_c) / 7) * -1;
    }

    while (pitch->note + interval < 'A') interval += 7;
    const char new_pitch = (char)('A' + (pitch->note + interval - 'A') % ('H' - 'A'));
    pitch->note = new_pitch;
    pitch->octave += octave_delta;
}

struct pitch
normalize_pitch(const struct pitch *pitch)
{
    struct pitch copy = *pitch;

    if (pitch->flag == 0) return copy;

    if (pitch->flag & FLAT)
    {
        add_to_pitch(&copy, -2);
        if (pitch->note == 'C' || pitch->note == 'F')
            copy.flag = 0;
        else
            copy.flag = SHARP;
    }

    else if (pitch->flag & SHARP)
    {
        if (pitch->note == 'E' || pitch->note == 'B')
        {
            add_to_pitch(&copy, 2);
            copy.flag = 0;
        }
    }

    return copy;
}

static bool
normalized_pitch_equals(const struct pitch lhs, const struct pitch rhs)
{
    return lhs.octave == rhs.octave &&
           lhs.note == rhs.note &&
           lhs.flag == rhs.flag;
}

bool
pitch_equals(const struct pitch *lhs, const struct pitch *rhs)
{
    const struct pitch norm_lhs = normalize_pitch(lhs);
    const struct pitch norm_rhs = normalize_pitch(rhs);
    return normalized_pitch_equals(
            norm_lhs, norm_rhs
    );
}

int
calculate_interval(struct pitch *higher, struct pitch *lower)
{
    /* TODO: Be smart, not stupid */
    size_t interval = 1;
    struct pitch mod_lower = *lower;
    while (! (mod_lower.note == higher->note && mod_lower.octave == higher->octave) )
    {
        add_to_pitch(&mod_lower, 2);
        ++interval;
    }
    return interval;
}

