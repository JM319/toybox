#include "image.h"
#include "gl_header.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <stdbool.h>

struct rectangle
rect_from_two_corners(float x0, float y0, float x1, float y1)
{
    struct rectangle result = {.x = x0, .y = y0,
                               .width = x1 - x0,
                               .height = y0 - y1};
    return result;
}

struct rectangle
rect_from_point_dims(float x, float y, float w, float h)
{
    struct rectangle result = {x, y, w, h};
    return result;
}

float 
rect_left(const struct rectangle* rectangle)
{
    return rectangle->x;
}

float 
rect_right(const struct rectangle* rectangle)
{
    return rectangle->x + rectangle->width;
}

float 
rect_top(const struct rectangle* rectangle)
{
    return rectangle->y;
}

float
rect_bottom(const struct rectangle* rectangle)
{
    return rectangle->y - rectangle->height;
}

void rectangle_vertices(const struct rectangle *rectangle,
                        float *vertices)
{
    vertices[0] = rect_left(rectangle);
    vertices[1] = rect_top(rectangle);

    vertices[2] = rect_right(rectangle);
    vertices[3] = rect_top(rectangle);

    vertices[4] = rect_right(rectangle);
    vertices[5] = rect_bottom(rectangle);

    vertices[6] = rect_left(rectangle);
    vertices[7] = rect_bottom(rectangle);
}

bool rectangle_contains_point(
    const struct rectangle *rectangle,
    float x, float y)
{
    return (y < rect_top(rectangle) && 
            y > rect_bottom(rectangle) &&
            x > rect_left(rectangle) &&
            x < rect_right(rectangle));
}

void compute_glyph_scale_and_offset(
    const struct clef_spec *spec,
    float coord_0, float coord_1,
    float *offset, 
    float *length)
{
    const float px_delta = (float)(spec->y_pixel_bottom - spec->y_pixel_top);
    const float coord_delta = coord_0 - coord_1;

    /* Get the size of the image in render coordinates by solving the equation

        px_delta           *length
       -----------  =   -------------
        image_dim        coord_delta
    */
    *length = spec->height_px * coord_delta / px_delta;
    
    /* Solving this equation gives us the offset to our first point:
      
       image_dim        offset
      ----------- =    --------
         px_0           *length
        
       And then we need to apply the coord offset
    */
    *offset = coord_0 + (spec->y_pixel_top * (*length) / spec->height_px);
}

void compute_image_rect_from_clef_spec(
    const struct clef_spec *spec,
    float top, float bottom,
    struct rectangle *rect)
{
    compute_glyph_scale_and_offset(spec,
            top, bottom,
            &rect->y,
            &rect->height);
    rect->x = spec->x_start;
    rect->width = spec->width;
}

void
load_clef_spec(const struct clef_spec *spec,
               float top, float bottom,
               struct image *image)
{
    compute_image_rect_from_clef_spec(
            spec,
            top, bottom,
            &image->rect);
    
    load_image(image, spec->pathname, &spec->uv_coordinates);
}

struct image_impl
{
    GLuint texture;
    GLuint buffers[2];
};

#define VERTEX_BUFFER buffers[0]
#define UV_BUFFER buffers[1]

void load_image_pixels(struct image *image, 
                const void *pixels,
                int width_px, int height_px, int format,
                const struct rectangle *uv_coordinates)
{
    stbi_set_flip_vertically_on_load(true);
    image->render_data = malloc(sizeof(struct image_impl));
    if (!image->render_data)
    {
        fprintf(stderr, "Out of memory\n");
        exit(1);
    }

    struct image_impl *impl = (struct image_impl *)image->render_data;
    glGenBuffers(2, impl->buffers);

    set_position(image, image->rect.x, image->rect.y);

    GLfloat note_uv[8] = {};
    rectangle_vertices(uv_coordinates, note_uv);
    glBindBuffer(GL_ARRAY_BUFFER, impl->UV_BUFFER);
    glBufferData(GL_ARRAY_BUFFER, sizeof(note_uv), note_uv, GL_STATIC_DRAW);

    glGenTextures(1, &impl->texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, impl->texture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, format, width_px, height_px,
                 0, format, GL_UNSIGNED_BYTE, pixels);
    glGenerateMipmap(GL_TEXTURE_2D);

}

void load_image(struct image *image, 
                const char *pathname,
                const struct rectangle *uv_coordinates)
{
    int width_px, height_px, channels;
    FILE* f = fopen(pathname, "r");
    if (!f)
    {
        fprintf(stderr, "Failed to read %s\n", pathname);
        exit(-1);
    }
    fclose(f);
    unsigned char *pixels = stbi_load(pathname, &width_px, &height_px, &channels, 0);
    if (!pixels)
    {
        fprintf(stderr, "Failed to load %s\n", pathname);
        exit(-1);
    }
    load_image_pixels(image, pixels, width_px, height_px, GL_RGBA, uv_coordinates);
    stbi_image_free(pixels);
}

void render_image(struct image *image)
{
    struct image_impl *impl = (struct image_impl *)image->render_data;
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, impl->texture);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, impl->VERTEX_BUFFER);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, impl->UV_BUFFER);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void set_position(struct image *image,
                  float new_x, float new_y)
{
    const struct rectangle image_rect = {new_x, new_y, image->rect.width, image->rect.height};
    set_image_rect(image, &image_rect);
}

void set_image_rect(struct image *image,
                    const struct rectangle* rect)
{
    image->rect = *rect;

    struct image_impl *impl = (struct image_impl *)image->render_data;
    GLfloat vertices[8] = {};
    rectangle_vertices(&image->rect, vertices);
    glBindBuffer(GL_ARRAY_BUFFER, impl->VERTEX_BUFFER);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
}

void set_uv_rect(struct image *image,
                 const struct rectangle *rect)
{
    struct image_impl *impl = (struct image_impl *)image->render_data;
    GLfloat vertices[8] = {};
    rectangle_vertices(rect, vertices);
    glBindBuffer(GL_ARRAY_BUFFER, impl->UV_BUFFER);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
}

#undef VERTEX_BUFFER
#undef UV_BUFFER