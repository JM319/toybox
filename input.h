#ifndef TOYBOX_INPUT_H
#define TOYBOX_INPUT_H

#include <stdbool.h>

/* Forward declaration */
struct game_state;

struct game_input
{
    float cursor_x;
    float cursor_y;

    bool is_down;
    bool was_down;
};

struct input_system
{
    /*
     * The two inputs are an attempt to keep interrupt-based input devices
     * from changing the input state while the game loop is executing.
     * dynamic_input is to be registered with the relevant callback functions.
     * Before starting the game loop, it gets copied/updated into static_input
     * which is used for all the loop calculations.
     */
    struct game_input static_input;
    struct game_input dynamic_input;
};

#define INPUT(s) (s)->input.static_input

/*
 * ------------------------------------------------------------
 * Functions for the platform layer to update the input state
 * ------------------------------------------------------------
 */
void set_cursor_position(struct input_system *input,
                         float x, float y);

void set_mouse_down(struct input_system *input,
                    bool is_down);

void initialize_input(int screen_width, int screen_height);

/*
 * Copy the current input state into static_input
 * This will be the working input state for the next frame
 */
void update_input(struct input_system *);


void normalize_input(struct input_system *);

#endif
