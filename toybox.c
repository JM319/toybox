#include "toybox.h"
#include "pitch.h"
#include "gl_header.h"
#include "stats.h"
#include "input.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#define RED 1.,0.,0.
#define BLACK 0.,0.,0.
#define WHITE 1.,1.,1.
#define GREEN 0.,1.,0.
#define YELLOW 1.,1.,0.

/*---------------------------- Math utils ----------------------------------*/
static int
rand_range(int low, int high)
{
    /* http://c-faq.com/lib/randrange.html */
    return low + rand() / (RAND_MAX / (high - low + 1) + 1);
}

void get_pixel_coordinates(float gl_x, float gl_y,
                           float screen_width, float screen_height,
                           float *px_x, float *px_y)
{
   *px_x = (screen_width * (gl_x + 1)) / 2;
   *px_y = screen_height - screen_height * (gl_y + 1) / 2;
}

void pixels_to_opengl_coordinates(float px_x, float px_y,
                                  float screen_width, float screen_height,
                                  float *gl_x, float *gl_y)
{
    *gl_x = -1 + (2 * px_x / screen_width);
    *gl_y = 1 - (2 * px_y / screen_height);
}

/* ---------------------------- Pitch functions ----------------------------- */

static struct pitch
pitch_from_offset(size_t offset, bool black_keys)
{
    static const struct pitch MIN_PITCH = {'F', 2, 0};
    struct pitch pitch = MIN_PITCH;
    add_to_pitch(&pitch, offset);

    /* Generate random sharp */
    if (black_keys)
    {
        const int sharp_test = rand_range(0, 4);
        if (sharp_test == 1 && pitch.note != 'E' && pitch.note != 'B') pitch.flag |= SHARP;
        if (sharp_test == 2 && pitch.note != 'C' && pitch.note != 'F') pitch.flag |= FLAT;
    }
    
    return pitch;
}

static const struct clef_spec treble_clef_spec = {
    .pathname = "./res/treble-clef.png",
    .height_px = 512,
    .y_pixel_top = 257, .y_pixel_bottom = 328,
    .uv_coordinates = {0.293f, 0.f, .707f, 1.f},
    .x_start = -0.6f, .width = .15f
};

static const struct clef_spec bass_clef_spec = {
    .pathname = "./res/bass-clef.png",
    .height_px = 256,
    .y_pixel_top = 0, .y_pixel_bottom = 74,
    .uv_coordinates = {0.f, 0.f, 1.f, 1.f},
    .x_start = -0.6f, .width = .1f
};

static const struct clef_spec whole_note_spec = {
    .pathname = "./res/whole-note.png",
    .height_px = 64,
    .y_pixel_top = 15, .y_pixel_bottom = 50,
    .uv_coordinates = {0.f, 0.f, 1.f, 1.f},
    .x_start = 0.0f, .width = 0.05f
};

static const struct clef_spec sharp_spec = {
    .pathname = "res/sharp.png",
    .height_px = 256,
    .y_pixel_top = 34, .y_pixel_bottom = 202,
    .uv_coordinates = {0.f, 0.f, 1.f, 1.f},
    .x_start = -0.13f, .width = 0.2f
};

static const struct clef_spec flat_spec= {
    .pathname = "res/flat.png",
    .height_px = 256,
    .y_pixel_top = 51, .y_pixel_bottom = 200,
    .uv_coordinates = {0.f, 0.f, 1.f, 1.f},
    .x_start = -0.08f, .width = 0.1f
};

static void
initialize_staff(struct staff *staff)
{
    glGenBuffers(1, &staff->buffer);
    glBindBuffer(GL_ARRAY_BUFFER, staff->buffer);

    static const GLfloat staff_points[] = {
        -0.6f, 0.9f,  1.0f, 0.9f,
        -0.6f, 0.85f, 1.0f, 0.85f,
        -0.6f, 0.8f,  1.0f, 0.8f,
        -0.6f, 0.75f, 1.0f, 0.75f,
        -0.6f, 0.7f,  1.0f, 0.7f,

        -0.6f, 0.5f,  1.0f, 0.5f,
        -0.6f, 0.45f, 1.0f, 0.45f,
        -0.6f, 0.4f,  1.0f, 0.4f,
        -0.6f, 0.35f, 1.0f, 0.35f,
        -0.6f, 0.30f, 1.0f, 0.30f,
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(staff_points), staff_points, GL_STATIC_DRAW);
}

static void
load_font(struct font *font, const char *font_pathname)
{
    const size_t buffer_size = 1u << 20u;
    unsigned char ttf_buffer[buffer_size];
    unsigned char temp_bitmap[512 * 512];
    FILE* font_file = fopen(font_pathname, "rb");
    fread(ttf_buffer, 1, buffer_size, font_file);
    fclose(font_file);

    stbtt_BakeFontBitmap(ttf_buffer, 0, 20.0f, temp_bitmap, 512, 512, 32, 96, font->cdata);
    struct rectangle uv = {0.f, 0.f, 1.f, 1.f};
    struct rectangle image_rect = {-1.f, 1.f, 1.f, 1.f};
    font->image.rect = image_rect;
    
    load_image_pixels(&font->image, temp_bitmap, 512, 512, GL_ALPHA, &uv);
}

static void
set_text(struct text *text, const char *label,
         float screen_width, float screen_height)
{
    float start_px_x, start_px_y;
    get_pixel_coordinates(text->x, text->y, screen_width, screen_height, &start_px_x, &start_px_y);

    text->num_letters = strlen(label);
    text->letter_rectangles = realloc(text->letter_rectangles, 
                                      2 * text->num_letters * sizeof(struct rectangle));
    if (text->letter_rectangles == NULL)
    {
        fprintf(stderr, "OOM\n");
        exit(1);
    }

    struct font *font = text->font;
    for (size_t ii = 0; ii < text->num_letters; ++ii)
    {
        struct rectangle *this_letter_rectangles = &text->letter_rectangles[2 * ii];
        stbtt_aligned_quad q;
        char ch = label[ii];
        stbtt_GetBakedQuad(font->cdata, 512, 512, ch - 32, &start_px_x, &start_px_y, &q, 1);
        float x0, y0, x1, y1;
        pixels_to_opengl_coordinates(q.x0, q.y0, screen_width, screen_height, &x0, &y0);
        pixels_to_opengl_coordinates(q.x1, q.y1, screen_width, screen_height, &x1, &y1);

        this_letter_rectangles[0] = rect_from_two_corners(x0, y0, x1, y1);
        this_letter_rectangles[1] = rect_from_two_corners(q.s0, q.t0, q.s1, q.t1);

        set_image_rect(&font->image, &this_letter_rectangles[0]);
        set_uv_rect(&font->image, &this_letter_rectangles[1]);
    }
}

static void
load_text(float start_x, float start_y,
          float screen_width, float screen_height,
          const char *label, struct text *text)
{
    text->x = start_x;
    text->y = start_y;

    set_text(text, label, screen_width, screen_height);
}

static void
render_staff(struct staff *staff)
{
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, staff->buffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_LINES, 0, 20);
    glDisableVertexAttribArray(0);
}

static void
render_text(struct text *text)
{
    for (size_t ii = 0; ii < text->num_letters; ++ii)
    {
        struct rectangle *this_letter_rectangles = &text->letter_rectangles[2 * ii];
        set_image_rect(&text->font->image, &this_letter_rectangles[0]);
        set_uv_rect(&text->font->image, &this_letter_rectangles[1]);
        render_image(&text->font->image);
    }
}

static void
generate_white_key(struct note *note)
{
    size_t new_offset = rand_range(1, 23);
    while (new_offset == note->offset)
        new_offset = rand_range(1, 23);

    note->offset = new_offset;
    note->pitch = pitch_from_offset(new_offset, false);
}

static void
generate_black_key(struct note *note)
{
    size_t new_offset = rand_range(1, 23);
    while (new_offset == note->offset)
        new_offset = rand_range(1, 23);

    note->offset = new_offset;
    note->pitch = pitch_from_offset(new_offset, true);
}

static bool
is_landmark(struct note *note)
{
    static const struct pitch MIDDLE_C = {'C', 4, 0};
    static const struct pitch TREBLE_G = {'G', 4, 0};
    static const struct pitch BASS_F = {'F', 3, 0};
    if (pitch_equals(&note->pitch, &MIDDLE_C) ||
        pitch_equals(&note->pitch, &TREBLE_G) ||
        pitch_equals(&note->pitch, &BASS_F))
        return true;
    return false;
}

static void
generate_landmark(struct note *note)
{
    struct pitch original_pitch = note->pitch;
    do
    {
        generate_white_key(note);
    } while (!is_landmark(note) || pitch_equals(&original_pitch, &note->pitch));
}

static void
set_generation_algorithm(struct game_state *state, note_generator algorithm)
{
    state->generate_note = algorithm;
}

static void
white_key_callback(struct game_state *state)
{
    set_generation_algorithm(state, generate_white_key);
}

static void
black_key_callback(struct game_state *state)
{
    set_generation_algorithm(state, generate_black_key);
}

static void
landmark_callback(struct game_state *state)
{
    set_generation_algorithm(state, generate_landmark);
}

static void
render_ledger_line(struct note *note)
{
    const float y = (rect_top(&note->image.rect) +
                     rect_bottom(&note->image.rect)) / 2.f;

    GLfloat points[] = 
    {
        rect_left(&note->image.rect) - .02f, y - .001f,
        rect_right(&note->image.rect) + .02f, y - .001f
    };

    glEnableVertexAttribArray(0);
    glVertexAttrib3f(1, BLACK);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, points);
    glDrawArrays(GL_LINES, 0, 2);
}

static void
update_note_position(struct note *note)
{
    static const float TREBLE_START_Y = 0.67f;
    static const float BASS_START_Y = 0.27f;
    static const float DELTA_Y = 0.025f;
    if (note->pitch.octave >= 4)
    {
        struct pitch baseline = {'C', 4, 0};
        const size_t interval = calculate_interval(&note->pitch, &baseline);
        set_position(&note->image,
                     note->image.rect.x,
                     TREBLE_START_Y + (DELTA_Y * interval));
    }
    else
    {
        struct pitch baseline = {'E', 2, 0};
        const size_t interval = calculate_interval(&note->pitch, &baseline);
        set_position(&note->image,
                     note->image.rect.x,
                     BASS_START_Y + (DELTA_Y * interval));
    }
}

static void
update_metrics(struct game_state *state)
{
    char label[100] = {};
    const int accuracy = compute_accuracy(&state->usage_stats);
    const float notes_per_second = compute_speed(&state->usage_stats);

    sprintf(label, "Accuracy: %d%s", accuracy, "%");
    set_text(&state->accuracy.text, label, (float)state->width, (float)state->height);

    sprintf(label, "Notes/s: %.02f", notes_per_second);
    set_text(&state->speed.text, label, (float)state->width, (float)state->height);
}

static void
ensure_initialization(struct game_state *state)
{
    if (!state->initialized)
    {
        initialize_usage_stats(&state->usage_stats);
        initialize_input(state->width, state->height);

        srand(time(0));
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        state->generate_note = generate_white_key;

        initialize_keyboard(state);
        initialize_staff(&state->staff);

        /* TODO: These magic numbers would be better expressed in terms of the staff */
        load_clef_spec(&whole_note_spec, .75f, .7f, &state->whole_note.image);
        load_clef_spec(&treble_clef_spec, .8f, .7f, &state->treble_clef);
        load_clef_spec(&bass_clef_spec, .5f, .45f, &state->bass_clef);
        load_clef_spec(&sharp_spec, .8f, .6f, &state->sharp);
        load_clef_spec(&flat_spec, .8f, .7f, &state->flat);

        load_font(&state->univers_condensed, "res/UniversCondensed.ttf");

        state->accuracy.text.font = &state->univers_condensed;
        load_text(-0.9f, 0.3f, (float)state->width, (float)state->height,
                  "Accuracy: 100", &state->accuracy.text);

        state->speed.text.font = &state->univers_condensed;
        load_text(-0.9f, 0.2f, (float)state->width, (float)state->height,
                  "Notes/s: 0", &state->speed.text);

        state->note_options.num_options = 3;
        /* TODO: This can fail */
        state->note_options.options = calloc(
                state->note_options.num_options, sizeof (struct option)
        );
        state->note_options.x = -1.f;
        state->note_options.y = 0.89f;
        state->note_options.rect_width = .075f;
        state->note_options.rect_height = .1f;

        struct option *landmark_option = &state->note_options.options[0];
        landmark_option->label = "Landmark";
        landmark_option->on_selected = landmark_callback;
        state->note_options.active_option = landmark_option;

        struct option *white_key_option = &state->note_options.options[1];
        white_key_option->label = "All white keys";
        white_key_option->on_selected = white_key_callback;

        struct option *black_key_option = &state->note_options.options[2];
        black_key_option->label = "Sharps & Flats";
        black_key_option->on_selected = black_key_callback;

        for (size_t option_index = 0; option_index < state->note_options.num_options; ++option_index)
        {
            struct option *option = &state->note_options.options[option_index];
            option->text.font = &state->univers_condensed;
            float rect_x = state->note_options.x;
            float rect_y = state->note_options.y - (option_index * (state->note_options.rect_height + .05f));
            option->rectangle = rect_from_point_dims(rect_x, rect_y, state->note_options.rect_width,
                                                     state->note_options.rect_height);
            load_text(option->rectangle.x + option->rectangle.width + .01f,
                      option->rectangle.y - (option->rectangle.height / 2) - .02f,
                      (float)state->width, (float)state->height,
                      option->label,
                      &option->text);
        }
        state->note_options.active_option->on_selected(state);

        state->generate_note(&state->whole_note);
        update_note_position(&state->whole_note);
        set_position(&state->sharp,
                     state->sharp.rect.x,
                     state->whole_note.image.rect.y + .1f);

        set_position(&state->flat,
                     state->flat.rect.x,
                     state->whole_note.image.rect.y + .07f);

        /*
         * Setup keydown listeners
         */
        register_listeners();

        state->found_key = false;
        state->initialized = true;
    }
}

static void
input(struct game_state *state)
{
    update_input(&state->input);
    normalize_input(&state->input);
}

static void
update(struct game_state *state)
{
    update_metrics(state);
    if (INPUT(state).was_down && !INPUT(state).is_down)
    {
        for (size_t option_index = 0; option_index < state->note_options.num_options; ++option_index)
        {
            struct option *option = &state->note_options.options[option_index];
            if (rectangle_contains_point(&option->rectangle,
                                         INPUT(state).cursor_x,
                                         INPUT(state).cursor_y))
            {
                state->note_options.active_option = option;
                option->on_selected(state);
            }
        }
    }

    if (INPUT(state).was_down && !INPUT(state).is_down && state->found_key)
    {
        state->found_key = false;
        state->generate_note(&state->whole_note);
        update_note_position(&state->whole_note);
        set_position(&state->sharp,
                     state->sharp.rect.x,
                     state->whole_note.image.rect.y + .1f);

        set_position(&state->flat,
                     state->flat.rect.x,
                     state->whole_note.image.rect.y + .06f);

    }

    update_keyboard(state);
}

static void
render(struct game_state *state)
{
    /* Render */
    glClear(GL_COLOR_BUFFER_BIT);

    /* Render images */
    glUseProgram(state->texture_program);
    glUniform1i(glGetUniformLocation(state->texture_program, "sTexture"), 0);
    render_image(&state->whole_note.image);
    if (state->whole_note.pitch.flag & SHARP)
        render_image(&state->sharp);
    else if (state->whole_note.pitch.flag & FLAT)
        render_image(&state->flat);
    render_image(&state->treble_clef);
    render_image(&state->bass_clef);

    for (size_t option_index = 0; option_index < state->note_options.num_options; ++option_index)
    {
        struct option *option = &state->note_options.options[option_index];
        render_text(&option->text);
    }

    render_text(&state->accuracy.text);
    render_text(&state->speed.text);

    glUseProgram(0);

    /* Render primitive-based artifacts */
    glUseProgram(state->program);

    glEnableVertexAttribArray(0);
    glVertexAttrib3f(1, BLACK);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    GLfloat lpoints[8];
    for (size_t option_index = 0; option_index < state->note_options.num_options; ++option_index)
    {
        struct option *option = &state->note_options.options[option_index];
        rectangle_vertices(&option->rectangle, lpoints);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, lpoints);
        if (option == state->note_options.active_option)
        {
            glVertexAttrib3f(1, GREEN);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glVertexAttrib3f(1, BLACK);
        }
        glDrawArrays(GL_LINE_LOOP, 0, 4);
    }

    static const struct pitch MIDDLE_C = {'C', 4, 0};
    if (pitch_equals(&state->whole_note.pitch, &MIDDLE_C))
        render_ledger_line(&state->whole_note);

    render_keyboard(state);
    render_staff(&state->staff);
    glUseProgram(0);
}

void toybox_draw(struct game_state *state)
{
    ensure_initialization(state);
    input(state);
    update(state);
    render(state);

    /* Report any errors */
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        printf("Error: %d\n", error);
    }
}
