#include "toybox.h"
#include <stdlib.h>
#include <stdio.h>

static const char VERTEX_SHADER[] =
"#version 100\n"
"attribute vec2 vPosition; \n"
"attribute vec3 aColor; \n"
"varying vec4 vColor;"
"void main() \n"
"{\n"
"    gl_Position = vec4(vPosition, 1., 1.);\n"
"    vColor = vec4(aColor, 1.);\n"
"}\n";

static const char FRAGMENT_SHADER[] =
"#version 100\n"
"precision mediump float;"
"varying vec4 vColor;\n"
"void main() \n"
"{\n"
"    gl_FragColor = vColor;\n"
"}\n";

static const char TEX_VERTEX_SHADER[] =
"#version 100\n"
"attribute vec2 aPosition; \n"
"attribute vec2 aTexCoord; \n"
"varying vec2 vTexCoord; \n"
"void main() \n"
"{\n"
"    gl_Position = vec4(aPosition, 1., 1.);\n"
"    vTexCoord = aTexCoord;\n"
"}\n";

static const char TEX_FRAGMENT_SHADER[] =
"#version 100\n"
"precision mediump float;"
"varying vec2 vTexCoord;\n"
"uniform sampler2D sTexture;\n"
"void main() \n"
"{\n"
"    gl_FragColor = texture2D(sTexture, vTexCoord);\n"
"}\n";

static GLuint load_shader(GLenum type, const char* source)
{
    GLuint shader = glCreateShader(type);
    if (shader == 0) return 0;

    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);

    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled)
    {
        GLint info_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
        if (info_len > 1)
        {
            char* info_log = malloc(info_len);
            glGetShaderInfoLog(shader, info_len, NULL, info_log);
            printf("Error compiling shader:\n%s\n", info_log);
            free(info_log);
        }
        glDeleteShader(shader);
        return 0;
    }
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        printf("Error: %d\n", error);
    }
    return shader;
}

static GLuint
load_program(const char *vertex_shader_src, const char *fragment_shader_src)
{
   const GLuint vertex_shader = load_shader(GL_VERTEX_SHADER, vertex_shader_src);
   const GLuint fragment_shader = load_shader(GL_FRAGMENT_SHADER, fragment_shader_src);

   GLuint program = glCreateProgram();
   if (program == 0) return 0;

   glAttachShader(program, vertex_shader);
   glAttachShader(program, fragment_shader);
   glBindAttribLocation(program, 0, "vPosition");
   glLinkProgram(program);

   GLint linked = 0;
   glGetProgramiv(program, GL_LINK_STATUS, &linked);

   if (!linked)
   {
       GLint info_len = 0;
       glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
       if (info_len > 0)
       {
           char* info_log = malloc(info_len);
           glGetProgramInfoLog(program, info_len, NULL, info_log);
           printf("Error linking program:\n%s\n", info_log);
           free(info_log);
       }
       glDeleteProgram(program);
       return 0;
   }

   return program;
}

void load_shaders(GLuint *program, GLuint *tex_program)
{
    *program = load_program(VERTEX_SHADER, FRAGMENT_SHADER);
    *tex_program = load_program(TEX_VERTEX_SHADER, TEX_FRAGMENT_SHADER);
}