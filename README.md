If you are reading this, you are probably a hiring manager following
a link from my resume.

You are probably wondering: Why is this code so awful?

This repository is half a useful teaching tool, and half an experiment.
I know that many programmers, perhaps game programmers more than others,
find that clean, small functions act as a hindrance and make things harder
to reason about. As I was making a game, I decided to set aside my opinions
and give their way a try.

I learned that I do not like working with giant functions and god objects.

Anyway, you can look at the last few commits for the beginning of my
process to de-"legacy" the code.

Anyway, see [this repo](https://gitlab.com/JM319/preprocessor) for an in-progress C89 preprocessor, developed with TDD, for something more indicative
of the type of code I normally write.
