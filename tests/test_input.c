#include "../input.h"
#include <stdio.h>
#include <stdlib.h>

static void die(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(-1);
}

#define ASSERT(expr) if (!(expr)) die("Test failed: " #expr)

static void test_update_input(void)
{
    struct input_system input;
    input.dynamic_input.is_down = true;
    input.dynamic_input.cursor_x = 1.0f;
    input.dynamic_input.cursor_y = 0.5f;
    update_input(&input);
    ASSERT(input.static_input.is_down);
    ASSERT(input.static_input.cursor_x == 1.0);
    ASSERT(input.static_input.cursor_y == 0.5);
}

static void test_flip_was_down(void)
{
    struct input_system input;
    input.static_input.is_down = true;
    input.dynamic_input.is_down = false;
    update_input(&input);
    ASSERT(input.static_input.was_down);
    update_input(&input);
    ASSERT(!input.static_input.was_down);
}

static void test_normalize_input(void)
{
    struct input_system input;
    initialize_input(10, 15);

    // Upper left corner
    input.static_input.cursor_x = 0.f;
    input.static_input.cursor_y = 0.f;

    normalize_input(&input);

    ASSERT(input.static_input.cursor_x == -1.f);
    ASSERT(input.static_input.cursor_y == 1.f);

    // Lower right corner
    input.static_input.cursor_x = 10.f;
    input.static_input.cursor_y = 15.f;

    normalize_input(&input);

    ASSERT(input.static_input.cursor_x == 1.f);
    ASSERT(input.static_input.cursor_y == -1.f);

    // Center
    input.static_input.cursor_x = 5.f;
    input.static_input.cursor_y = 7.5f;

    normalize_input(&input);

    ASSERT(input.static_input.cursor_x == 0.f);
    ASSERT(input.static_input.cursor_y == 0.f);
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    test_update_input();
    test_flip_was_down();
    test_normalize_input();

    return 0;
}
