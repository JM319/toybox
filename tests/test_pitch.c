#include "../pitch.h"
#include <stdio.h>
#include <stdlib.h>

static void die(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(-1);
}

#define ASSERT(expr) if (!(expr)) die("Test failed: " #expr)

static void test_normalize_unflagged_pitch(void)
{
    struct pitch original = {'A', 5, 0};
    struct pitch copy = normalize_pitch(&original);
    ASSERT(copy.note == 'A');
    ASSERT(copy.octave == 5);
    ASSERT(copy.flag == 0);
}

static void test_sharp_white_key(void)
{
    struct pitch original = {'B', 5, SHARP};
    struct pitch copy = normalize_pitch(&original);
    ASSERT(copy.note == 'C');
    ASSERT(copy.octave == 6);
    ASSERT(copy.flag == 0);
}

static void test_flat(void)
{
    struct pitch original = {'B', 5, FLAT};
    struct pitch copy = normalize_pitch(&original);
    ASSERT(copy.note == 'A');
    ASSERT(copy.octave == 5);
    ASSERT(copy.flag == SHARP);
}

static void test_add_intervals(void)
{
    struct pitch pitch = {'B', 5, FLAT};
    add_to_pitch(&pitch, 1);
    ASSERT(pitch.note == 'B');

    add_to_pitch(&pitch, 2);
    ASSERT(pitch.note == 'C');
    ASSERT(pitch.octave == 6);

    add_to_pitch(&pitch, 5);
    ASSERT(pitch.note == 'G');
    ASSERT(pitch.octave == 6);

    add_to_pitch(&pitch, 8);
    ASSERT(pitch.note == 'G');
    ASSERT(pitch.octave == 7);
}

static void
test_subtract_intervals(void)
{
    struct pitch pitch = {'B', 5, 0};
    add_to_pitch(&pitch, -1);
    ASSERT(pitch.note == 'B');

    add_to_pitch(&pitch, -7);
    ASSERT(pitch.note == 'C');
    ASSERT(pitch.octave== 5);

    add_to_pitch(&pitch, -2);
    ASSERT(pitch.note == 'B');
    ASSERT(pitch.octave== 4);

    add_to_pitch(&pitch, -8);
    ASSERT(pitch.note == 'B');
    ASSERT(pitch.octave== 3);

    add_to_pitch(&pitch, -5);
    ASSERT(pitch.note == 'E');
    ASSERT(pitch.octave== 3);
}

static void
test_compare_enharmonic_notes()
{
    struct pitch lhs = {'B', 5, SHARP};
    struct pitch rhs = {'C', 6, 0};
    ASSERT(pitch_equals(&lhs, &rhs));

    rhs.octave = 5;
    ASSERT(!pitch_equals(&lhs, &rhs));

    lhs.flag = 0;
    rhs.note = 'C';
    rhs.flag = FLAT;
    rhs.octave = 6;
    ASSERT(pitch_equals(&lhs, &rhs));

    rhs.flag = SHARP;
    ASSERT(!pitch_equals(&lhs, &rhs));
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    test_normalize_unflagged_pitch();
    test_sharp_white_key();
    test_flat();
    test_add_intervals();
    test_subtract_intervals();
    test_compare_enharmonic_notes();
    return 0;
}
