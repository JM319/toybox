#ifndef TOYBOX_H
#define TOYBOX_H

/* TODO: Don't want this so public!! */
#include "stats.h"
#include "gl_header.h"
#include "image.h"
#include "input.h"
#include "stb_truetype.h"
#include <stdbool.h>
#include "pitch.h"

enum state 
{
    KEY_DEFAULT,
    KEY_TOUCH,
    KEY_WRONG,
    KEY_RIGHT
};

enum key_type
{
    WHITE_KEY,
    BLACK_KEY
};

struct white_key
{
    struct rectangle base;
    struct rectangle tip;

    /* Members for managing OpenGL-related state */
    GLuint buffers[3]; /* buffers for base, tip, and outline */
};

struct black_key
{
    struct rectangle rect;

    GLuint buffer;
};

struct key
{
    enum state state;
    struct pitch pitch;
    enum key_type type;
    void *key_drawable;
};

struct staff
{
    GLuint buffer;
};

struct note
{
    struct image image;
    struct pitch pitch;
    size_t offset;
};

struct font
{
    struct image image;
    stbtt_bakedchar cdata[96];
};

struct text
{
    float x, y;
    size_t num_letters;
    struct rectangle *letter_rectangles;
    struct font *font;
};

struct metric
{
    struct text text;
    int value;
};

struct game_state;
typedef void (*option_callback)(struct game_state *state);

struct option
{
    struct text text;
    const char *label;
    struct rectangle rectangle;
    option_callback on_selected;
};

typedef void (*note_generator)(struct note *);

struct option_group
{
    float x;
    float y;
    float rect_width;
    float rect_height;

    size_t num_options;
    struct option *options;
    struct option *active_option;
};

/* --------------------
 * Listener funcs
 * --------------------
 */
void register_listeners();
void execute_listeners(struct game_state *game, struct key *key);

struct game_state
{
    /* screen dimensions */
    int width, height;
    struct usage_stats usage_stats;
    
    struct input_system input;

    GLuint program;
    GLuint texture_program;

    struct note whole_note;
    struct image treble_clef;
    struct image bass_clef;
    struct image sharp;
    struct image flat;

    struct key keys[40];

    struct option_group note_options;
    note_generator generate_note;
    struct font univers_condensed;

    struct metric accuracy;
    struct metric speed;

    struct staff staff;

    bool initialized;  /* Whether state has been initialized. */
    bool found_key;
};

void toybox_draw(struct game_state *);
void load_shaders(GLuint *, GLuint *);
void initialize_keyboard(struct game_state *);
void update_keyboard(struct game_state *);
void render_keyboard(struct game_state *state);

bool pitch_equals(const struct pitch *lhs, const struct pitch *rhs);
void add_to_pitch(struct pitch *pitch, int interval);

#define ARRAY_COUNT(A) (sizeof((A)) / sizeof((A)[0]))

#endif